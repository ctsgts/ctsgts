import sys
import json
from datetime import datetime
import FailCaseMapper
from ReadDataBaseCTS import setListData
lastline = ""
timeresult = ""
dict_result = {}
def readLog(str_Path):
    list_lines = []
    # read all lines in log as a list    
    file = open(str_Path, 'r')
    for line in file:
        list_lines.append(line)
    file.close()
    return list_lines

def value(str, key):
    return str.partition(key)[2].split('"')[1]
    
def listResultline(line,lastline,timeresult,projresult,secutiryPatch,fingerPrint,releaseSDK,hostInfo,suiteBuild,suitePlan):
    reasonlist = []
    infoList = []
    data = {}
    
  
    if "Test result=\"fail\"" in line:        	  
        targetline = line
        print("lastline: "+lastline)
        print("Test result: "+timeresult)
        print("time: "+timeresult)
        #info = line
        #infoList.append(lastline)
        dict_result['SecutiryPatch'] = secutiryPatch
        info = dict_result['SecutiryPatch']  
        infoList.append(info)
        dict_result['FingerPrint'] = fingerPrint
        info = dict_result['FingerPrint']  
        infoList.append(info)
        dict_result['ReleaseSDK'] = releaseSDK
        info = dict_result['ReleaseSDK']  
        infoList.append(info)
        dict_result['HostInfo'] = hostInfo
        info = dict_result['HostInfo']  
        infoList.append(info)
        dict_result['SuiteBuild'] = suiteBuild
        info = dict_result['SuiteBuild']  
        infoList.append(info)
        dict_result['SuitePlan'] = suitePlan
        info = dict_result['SuitePlan']  
        infoList.append(info)
        dict_result['Project'] = projresult;    
        info = dict_result['Project']  
        infoList.append(info)
        dict_result['Date'] = timeresult;    
        info = dict_result['Date']  
        infoList.append(info)
        dict_result['TestCase'] = lastline.split('"')[1]
        info = dict_result['TestCase']
        infoList.append(info)
        print("TestCase: "+dict_result['TestCase'])
        dict_result['TestItem'] = targetline.split('"')[3]
        info = dict_result['TestItem']
        infoList.append(info)
        
    if " abi=\"" in line:
    		dict_result['abi'] = line.split('"')[3]
    		info = dict_result['abi']
    		infoList.append(info)
    		
    		        
    if "Failure message" in line:
        targetline = line
        #info = line
        dict_result['Message'] = line.split('"')[1].replace("'", "")
        info = dict_result['Message']
        #targetline.split('\"')[1]
        infoList.append(info)
        
    if "<StackTrace" in line:
        targetline = line
        #info = line
        dict_result['Detail'] = targetline.split('>')[1].replace("'", "")
        info = dict_result['Detail']
        infoList.append(info)            

        
    return dict_result
        
def dumpCriticalInfoToDict(data):
	
    fields ={}
    print("data: "+str(data))
    #SecutiryPatch
    fields.update(SecutiryPatch = data['SecutiryPatch'] )
    #FingerPrint
    fields.update(FingerPrint = data['FingerPrint'] )
    #ReleaseSDK
    fields.update(ReleaseSDK = data['ReleaseSDK'] )
    #HostInfo
    fields.update(HostInfo = data['HostInfo'] )
    #SuiteBuild
    fields.update(SuiteBuild = data['SuiteBuild'] )
    #SuitePlan
    fields.update(SuitePlan = data['SuitePlan'] )
    #abi
    fields.update(abi = data['abi'] )
    #Project
    fields.update(Project = data['Project'] )
    #Date
    fields.update(Date = data['Date'] )
    #TestCase
    fields.update(TestCase = data['TestCase'] )
    #TestItem
    fields.update(TestItem = data['TestItem'] )
    #Message
    fields.update(Message = data['Message'] )
    #Detail
    fields.update(Detail = data['Detail'] )
    
    return fields;  # retrun a dict    
    
def printCTSResult(dict_result):

    print("printCTSResult: "+str(dict_result))
    return

def printResult(dict_result):
    # print your result as the format described on power point
    # print 'no match' if you did not find anything
   # print("TestCase: "+dict_result['TestCase'])
    print("FailureMsg: "+dict_result['FailureMsg'])
    print("Reason:")
    for line in dict_result['Reason']:
        print("--->" + line)
    return
        
def outputToFile(infoList):
    fp = open("/home/fihtdc/Workspace/Mina/ctsgts/output.txt", "w")
    fp.write("[\n")
    for info in infoList:
        fp.write(json.dumps(info))
        fp.write(",\n")
    fp.write("]")
    fp.close()
def main():
    print ('[Find Wathdog in Log]')
    resultList = []
    cts_dict_result = {}
    lastline = ""
    timeresult = ""
    build_device = ""
    fingerPrint = ""
    secutiryPatch = ""
    releaseSDK = ""
    suitePlan = ""
    suiteBuild = ""
    hostInfo = ""
    abi = ""
    
    #list_log = readLog('C:\\python37\\test_result.xml')
    print ('argument list: ', str(sys.argv))
    list_log = readLog(sys.argv[1])
    for line in list_log:
    	if "Result start=" in line:
    		timestamp = line.split('"')[1];
    		time = datetime.utcfromtimestamp(int(timestamp[:10]))
    		timeresult = str(time);
    		suitePlan = value(line, 'suite_name') + '/' + value(line, 'suite_plan')
    		suiteBuild = value(line, 'suite_version') + '/' + value(line, 'suite_build_number');
    		hostInfo = value(line, 'host_name') + ' (' + value(line, 'os_name') + ' - ' + value(line, 'os_version') + ')';
    		print("time: "+timeresult)
    		print("suitePlan: "+suitePlan)
    		print("suiteBuild: "+suiteBuild)
    		print("hostInfo: "+hostInfo)
    		
    	if "build_device=" in line:
    		build_device = line.split('"')[25];
    		fingerPrint = value(line, 'build_fingerprint');
    		secutiryPatch = value(line, 'build_version_security_patch');
    		releaseSDK = value(line, 'build_version_release') + ' (' + value(line, 'build_version_sdk') + ')';    		
    		print("proj: "+build_device)
    		print("fingerPrint: "+fingerPrint)
    		print("secutiryPatch: "+secutiryPatch)
    		print("releaseSDK: "+releaseSDK)

 
 		
    		
    #list_log = readLog('C:\\python37\\test_result.xml')
    print ('argument list: ', str(sys.argv))
    list_log = readLog(sys.argv[1])

    for line in list_log:
    	dict_result = listResultline(line,lastline,timeresult,build_device,secutiryPatch,fingerPrint,releaseSDK,hostInfo,suiteBuild,suitePlan)
    	if "TestCase name=" in line:
    		lastline = line;
    	
    	if "<StackTrace" in line:
    		printCTSResult(dict_result)
    		#outputToFile(dict_result) 
    		
    		info = dumpCriticalInfoToDict(dict_result)#issue.raw means JSON format
    		resultList.append(info)
    		dict_result = {}
    

    setListData(resultList)

    #Mapping fail case to Jira db
    dbName = "test"
    testSuite = "cts"
    for item in resultList:
        testCase = item['TestCase']
        FailCaseMapper.mapping_data_and_insert_db(testCase, dbName, testSuite)
        
    #outputToFile(resultList)    

if __name__ == '__main__':
    main()
 
  