# -*- coding: utf-8 -*-
import pymysql
import time
import os

import ReadDataBase

dbName = "test"
testSuite = ""

def search_fail_case_from_jira(fail_case):
    project_list = []
    root = os.path.dirname(os.path.abspath(__file__))
    fp = open(root + "/project_list.txt", "r")
    project_list = fp.read().splitlines()
    fp.close()

    dbOP = ReadDataBase.DBOperator()
    dbOP.updateDBName(dbName)
    dbOP.databaseInitial()

    mapping_list = []
    for project in project_list:
        jira_list = dbOP.selectData(project)
        for jira in jira_list:            
            if fail_case in jira[12] or fail_case in jira[16]:
                #print(jira)
                mapping_list.append(jira)

    dbOP.closeDB()

    return mapping_list

def insert_to_database(fail_case, mapping_list):
    date_string = time.strftime("%Y%m%d", time.localtime())
    dbOP = ReadDataBase.DBOperator()
    dbOP.updateDBName(dbName)
    

    if "cts" in testSuite:
        dbOP.switchDBTable("cts_mapping_table")
    elif "gts" in testSuite:
        dbOP.switchDBTable("gts_mapping_table")
    else:
        print("Wrong test suite. Please check again.")
        return

    dbOP.databaseInitial()

    query_dict = {"TestCase" : fail_case}
    query_result = dbOP.selectDataDynamic(query_dict)

    for jira in mapping_list:
        pr_number = jira[1]

        for item in query_result:
            if item[0] == fail_case and item[1] == pr_number:
                break
        else:
            insert_dict = {"TestCase":fail_case, "pr_number":pr_number, "Date":date_string}
            dbOP.insertData(insert_dict)
            print("Insert to mapping_table. insert data:")
            print(insert_dict)

    dbOP.closeDB()

def mapping_data_and_insert_db(fail_case, db, ts):
    global dbName
    global testSuite
    dbName = db
    testSuite = ts
    mapping_list = search_fail_case_from_jira(fail_case)
    insert_to_database(fail_case, mapping_list)

def mapping_all_data(db):
    dbOP = ReadDataBase.DBOperator()
    dbOP.updateDBName(db)
    dbOP.databaseInitial()

    ts = "cts"
    dbtable = "cts_xml"
    print("All CTS fail item.")

    dbOP.switchDBTable(dbtable)
    all_data = dbOP.selectAll()
    for data in all_data:
        fail_case = data[2]
        print("project:" + data[0] + ", testcase:" + fail_case)
        mapping_data_and_insert_db(fail_case, db, ts)

    ts = "gts"
    dbtable = "gts_xml"
    print("All GTS fail item.")

    dbOP.switchDBTable(dbtable)
    all_data = dbOP.selectAll()
    for data in all_data:
        fail_case = data[2]
        print("project:" + data[0] + ", testcase:" + fail_case)
        mapping_data_and_insert_db(fail_case, db, ts)

    dbOP.closeDB()