from jira import JIRA
from jira.client import JIRA
import json
import time
import os
import sys

import ReadDataBase

class JiraOperator():
    def __init__(self):
        self.username = 'allproject_user'
        self.password = 'Foxconn123'
        self.isConnect = False

    def connect(self):
        options = {'server': 'https://tpe-jira2.fihtdc.com/'}
        print("Log in to Jira...")
        try:
            self.jira = JIRA(options, basic_auth=(self.username, self.password))
            self.isConnect = True
            print("Login successful.")
        except:
            print("Login failed. Please check connection.")

    def query(self, project, keyword, withinDays):
        jql = "project = " + project + " AND text ~ " + keyword
        if withinDays.lower() != "all":
            jql = jql + " AND (created >= -" + str(withinDays) + "d OR updated >= -" + str(withinDays) + "d)"
        print("Query jira... jql: " + jql)
        issues = self.jira.search_issues(jql, maxResults=1000)
        print("Query jira successful.")

        return issues

    def isProject(self, project):
        projects = self.jira.projects()

        for project_data in projects:
            #print("project_data.name:" + str(project_data.name))
            if project == project_data.name:
                return True
        else:
            return False

    def closeJira(self):
        self.jira.close()


def dumpCriticalInfo(data):
    fields ={}

    #PR
    fields.update(pr = data['key'][:3] )
    fields.update(pr_number = data['key'] )
    #Status
    fields.update(status = data['fields']['status']['name'] )
    #Assignee
    fields.update(assignee = data['fields']['assignee']['name'] )
    #Owner
    if data['fields']['customfield_11080'] is not None:
        fields.update(owner = data['fields']['customfield_11080']['name'] )
    else:
        fields.update(owner = None)
    #Severity
    fields.update(severity = data['fields']['priority']['name'] )
    #Priority
    if data['fields']['customfield_10239'] is not None:
        fields.update(priority = data['fields']['customfield_10239']['value'] )
    else:
        fields.update(priority = None)
    #Team
    fields.update(team = data['fields']['customfield_11789'])
    #Created
    fields.update(created = data['fields']['created'] )
    #DueDay
    fields.update(dueDay = data['fields']['duedate'] )
    #Summary
    fields.update(summary = data['fields']['summary'])
    #MustFix
    if data['fields']['customfield_17080'] is not None:
        fields.update(mustFix = data['fields']['customfield_17080']['value'])
    else:
        fields.update(mustFix = None)
    #MustFixMR
    if (data['fields']['customfield_17080'] is not None) and\
        ('child' in data['fields']['customfield_17080']):
            fields.update(mustFixMR = data['fields']['customfield_17080']['child']['value'] )
    else:
        fields.update(mustFixMR = None)
    #Issue_white
    fields.update(issue_white = data['fields']['customfield_10790'])
    #Description
    fields.update(description = data['fields']['description'])
    #Integrate_ver
    fields.update(integrate_ver = data['fields']['customfield_10274'] )
    #Build_model
    if data['fields']['customfield_10065'] is not None:
        build_model = ""
        customfield_10065 = data['fields']['customfield_10065']
        if customfield_10065 is not None:
            for i in range(len(customfield_10065)):
                build_model = build_model + customfield_10065[i]['value'] + ", "
            build_model = build_model[:-2]
            fields.update(build_model = build_model)
    else:
        fields.update(build_model = None)
    #Type
    fields.update(type = data['fields']['issuetype']['name'])
    #Google Bug Link
    fields.update(google_bug_link = data['fields']['customfield_22285'])

    return fields


if __name__ == '__main__':
    root = os.path.dirname(os.path.abspath(__file__))
    date_string = time.strftime("%Y%m%d", time.localtime())
    dbOP = ReadDataBase.DBOperator()
    withinDays = "all"
    
    # Parse parameter
    for para in sys.argv:
        if "db=" in para:
            dbName = para.replace("db=", "")
            dbOP.updateDBName(dbName)
        elif "withinDays=" in para:
            withinDays = para.replace("withinDays=", "")

    # Initial DB
    dbOP.databaseInitial()

    # Initial jira
    jiraOP = JiraOperator()
    jiraOP.connect()
    if not jiraOP.isConnect:
        sys.exit(0)

    # Get project list
    project_list = []
    fp = open(root + "/project_list.txt", "r")
    project_list = fp.read().splitlines()
    fp.close()
    print("project_list: " + str(project_list))

    # Get keyword list
    keyword_list = []
    fp = open(root + "/keyword_list.txt", "r")
    keyword_list = fp.read().splitlines()
    fp.close()
    print("keyword_list: " + str(keyword_list))

    # check data folder is exist
    data_folder_path = root + "/data"
    if not os.path.exists(data_folder_path):
        print("Data folder is not exist, create data folder.")
        os.makedirs(data_folder_path)

    # Query Jira
    for project in project_list:
        if not jiraOP.isProject(project):
            print(str(project) + " is not project name.")
            continue

        for keyword in keyword_list:
            issues = jiraOP.query(project, keyword, withinDays)
            file_name = project + "_" + keyword + "_" + date_string + ".txt"
            file_path = data_folder_path + "/" + file_name
            fp = open(file_path, "w")
            issue_list = []
            for issue in issues:
                fp.write('{:12s}{}\n'.format(issue.key, issue.fields.summary))
                data_dict = dumpCriticalInfo(issue.raw)
                data_dict.update(key_word = keyword)
                data_dict.update(date_updated_db = date_string)
                # insert to db
                pr_number = data_dict["pr_number"]
                result = dbOP.selectDataDynamic({"pr_number":pr_number})
                if len(result) is not 0:
                    dbOP.updateData(data_dict, {"pr_number":pr_number})
                else:
                    dbOP.insertData(data_dict)

            fp.close()
            print("Gen file " + file_path  + " successful.")

    print("Query finish.")

    dbOP.closeDB()
    jiraOP.closeJira()
    print("Close DB & Jira connection.")
